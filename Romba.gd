extends KinematicBody2D
var velocity = Vector2(0, 500)



func _ready():
	set_process(true)



func _process(delta):
	move_and_slide(velocity)
	if(is_on_wall()):
		rotate(PI)
		velocity *= -1

