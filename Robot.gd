extends KinematicBody2D

var veloc = Vector2.ZERO
var accel = 50
var decel = 100
var max_speed = 100

func exit_level():
	pass

func get_input():
	var dir = Vector2.ZERO
	if Input.is_action_pressed('right'):
		dir.x += 1
	if Input.is_action_pressed('left'):
		dir.x -= 1
	if Input.is_action_pressed('down'):
		dir.y += 1
	if Input.is_action_pressed('up'):
		dir.y -= 1
	return dir.normalized()

func _physics_process(delta):
	var dir = get_input()
	if dir == Vector2.ZERO:
		if veloc.length() > decel * delta:
			veloc -= veloc.normalized() * decel * delta
		else:
			veloc = Vector2.ZERO
	else:
		move(dir * accel * delta)
	veloc = move_and_slide(veloc)

func move(vroom):
	veloc += vroom
	veloc = veloc.clamped(max_speed)
