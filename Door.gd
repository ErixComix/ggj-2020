extends RigidBody2D

export var is_exit = false
var velY = Vector2(0, 18)
var counter = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not is_exit:
		if floor(counter / 20) == 0:
			set_global_position(get_global_position() - velY)
			counter += 1
		elif floor(counter / 20) == 1:
			set_global_position(get_global_position() + velY)
			counter += 1
		else:
			counter = 0

func _on_SlidingDoorTemplate_body_entered(body):
	if body.get_class() == "KinematicBody2D":
		get_tree().change_scene("res://Level_5.tscn")
