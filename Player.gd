extends KinematicBody2D
export var speed = 400
var extents
var screensize
var velocity = Vector2()
var pos = Vector2()
var input = Vector2()


# Called when the node enters the scene tree for the first time.
func _ready():
	extents = 32 #get_node("PlayerSprite").get_texture().get_size() / 2
	pos = get_global_position()
	set_process(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$PlayerSprite.play()
	if(Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_up")):
		var newpos = pos
		newpos.x = pos.x + speed / sqrt(2) * delta
		newpos.y = pos.y - speed / sqrt(2) * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = true
		$PlayerSprite.flip_v = false
		set_global_rotation(0)
	elif(Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_down")):
		var newpos = pos
		newpos.x = pos.x + speed / sqrt(2) * delta
		newpos.y = pos.y + speed / sqrt(2) * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = true
		$PlayerSprite.flip_v = true
		set_global_rotation(0)
	elif(Input.is_action_pressed("ui_left") and Input.is_action_pressed("ui_up")):
		var newpos = pos
		newpos.x = pos.x - speed / sqrt(2) * delta
		newpos.y = pos.y - speed / sqrt(2) * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = false
		$PlayerSprite.flip_v = false
		set_global_rotation(0)
	elif(Input.is_action_pressed("ui_left") and Input.is_action_pressed("ui_down")):
		var newpos = pos
		newpos.x = pos.x - speed / sqrt(2) * delta
		newpos.y = pos.y + speed / sqrt(2) * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = false
		$PlayerSprite.flip_v = true
		set_global_rotation(0)
	elif(Input.is_action_pressed("ui_right") and not Input.is_action_pressed("ui_left")):
		var newpos = pos
		newpos.x = pos.x + speed * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.stop()
		set_global_rotation(get_global_rotation() + 2.7 * PI * delta)
	elif(Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right")):
		var newpos = pos
		newpos.x = pos.x - speed * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.stop()
		set_global_rotation(get_global_rotation() - 2.7 * PI * delta)
	elif(Input.is_action_pressed("ui_up") and not Input.is_action_pressed("ui_down")):
		var newpos = pos
		newpos.y = pos.y - speed * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = false
		$PlayerSprite.flip_v = false
		set_global_rotation(0)
	elif(Input.is_action_pressed("ui_down") and not Input.is_action_pressed("ui_up")):
		var newpos = pos
		newpos.y = pos.y + speed * delta
		if not test_move(Transform2D(0, newpos), Vector2.ZERO):
			pos = newpos
			set_global_position(pos)
		$PlayerSprite.flip_h = false
		$PlayerSprite.flip_v = true
		set_global_rotation(0)
	else:
		$PlayerSprite.stop()
	#set_global_position(pos)
 
